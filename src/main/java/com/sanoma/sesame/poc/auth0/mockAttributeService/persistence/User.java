package com.sanoma.sesame.poc.auth0.mockAttributeService.persistence;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Data @AllArgsConstructor
@Entity
@Table(name = "user")
public class User {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", nullable = false)
    private Long id;
 
    @Column(name = "email", nullable = false)
    private String email;
 
    @OneToMany(mappedBy = "user")
    private Set<License> license;
}
