package com.sanoma.sesame.poc.auth0.mockAttributeService.repository;

import org.springframework.data.repository.CrudRepository;
import com.sanoma.sesame.poc.auth0.mockAttributeService.persistence.License;

public interface LicenseRepository extends CrudRepository<License, Long> {
}