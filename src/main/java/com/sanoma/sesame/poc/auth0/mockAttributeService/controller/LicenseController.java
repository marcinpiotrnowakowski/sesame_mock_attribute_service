package com.sanoma.sesame.poc.auth0.mockAttributeService.controller;

import java.util.Arrays;
import java.util.List;
import com.sanoma.sesame.poc.auth0.mockAttributeService.persistence.License;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/license")
public class LicenseController {

	@GetMapping
	public List<License> list() {
        return Arrays.asList(
            new License(1L, "license-1001", null),
            new License(2L, "license-1002", null),
            new License(3L, "license-1003", null));
    }

    @GetMapping("/{id}")
	public License getrById(@PathVariable("id") Long id) {
		return new License(id, "license-2001", null);
	}
    
    @PostMapping
	public License save(@RequestBody License license) {
		return license;
	}
}