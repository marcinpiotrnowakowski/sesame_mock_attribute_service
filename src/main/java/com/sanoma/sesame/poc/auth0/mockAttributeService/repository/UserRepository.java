package com.sanoma.sesame.poc.auth0.mockAttributeService.repository;

import org.springframework.data.repository.CrudRepository;
import com.sanoma.sesame.poc.auth0.mockAttributeService.persistence.User;

public interface UserRepository extends CrudRepository<User, Long> {
}