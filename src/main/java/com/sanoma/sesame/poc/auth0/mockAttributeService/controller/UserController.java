package com.sanoma.sesame.poc.auth0.mockAttributeService.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.sanoma.sesame.poc.auth0.mockAttributeService.persistence.License;
import com.sanoma.sesame.poc.auth0.mockAttributeService.persistence.User;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/user")
public class UserController {
	Map<String, String> user2licenseMap = Stream.of(new String[][] {
		{ "game.of.no@gmail.com", "license-1001"}, 
		{ "marcin.piotr.nowakowski@gmail.com", "license-1002"},
		{ "rafal.laskowski@outlook.com", "license-1003"}, 
		{ "r.laskowski@nowaera.pl", "license-1004"}
	}).collect(Collectors.toMap(data -> data[0], data -> data[1]));

	@GetMapping
	public List<User> list(@RequestParam Optional<String> email) {
		if(!email.isPresent()) {
			return user2licenseMap
				.entrySet()
				.stream()
				.map(this::createUserWithLicense)
				.collect(Collectors.toList());
		} else if(user2licenseMap.containsKey(email.get())) {
			return List.of(createUserWithLicense(email.get(), user2licenseMap.get(email.get())));
		} else {
			return List.of();
		}
    }

	@GetMapping("/{id}")
	public User getrById(@PathVariable("id") Long id) {
		return new User(id, "get.by.id@gmail.com", null);
	}
    
    @PostMapping
	public User save(@RequestBody User user) {
		return user;
	}

	private User createUserWithLicense(Map.Entry<String, String> user2license) {
		return createUserWithLicense(user2license.getKey(), user2license.getValue());
	}

	private User createUserWithLicense(String email, String licenseNumber) {
		return new User(1L, email, Set.of(new License(1L, licenseNumber, null)));
	}
}
