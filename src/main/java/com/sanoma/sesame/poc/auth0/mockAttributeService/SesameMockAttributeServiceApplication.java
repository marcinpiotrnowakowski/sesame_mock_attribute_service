package com.sanoma.sesame.poc.auth0.mockAttributeService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SesameMockAttributeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SesameMockAttributeServiceApplication.class, args);
	}

}
