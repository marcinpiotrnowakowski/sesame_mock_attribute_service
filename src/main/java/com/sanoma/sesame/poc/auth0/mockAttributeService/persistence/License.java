package com.sanoma.sesame.poc.auth0.mockAttributeService.persistence;

import lombok.AllArgsConstructor;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data @AllArgsConstructor
@Entity
@Table(name = "license")
public class License {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "license_id", nullable = false)
    private Long id;
 
    @Column(name = "number", nullable = false)
    private String number;
 
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
