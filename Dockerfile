FROM adoptopenjdk:11-jre-hotspot
ARG BUILD=build
RUN mkdir /opt/app
COPY ${BUILD}/libs/mockAttributeService-0.0.1-SNAPSHOT.jar /opt/app/mockAttributeService.jar
CMD ["java", "-jar", "/opt/app/mockAttributeService.jar"]
