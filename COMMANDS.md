# build image locally
docker-compose build

# run container locally
docker-compose up sesamesanoma-mock-attribute-serice

# list available docker images
docker images

# cleans docker cache
docker system prune -a

# tag image with fully quantified name
docker tag sesamesanoma.docker-local/mock-attribute-service sesamesanoma.azurecr.io/mock-attribute-service-snapshot-20200603_150001

# log in to azure account
az login

# log in to azure container registry
az acr login --name sesamesanoma.azurecr.io

# push image to container registry
docker push sesamesanoma.azurecr.io/mock-attribute-service-snapshot-20200603_150001